<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'blogirie');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'blogirie');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'Blo123qwe@');

/** Nome do host do MySQL */
define('DB_HOST', 'blogirie.mysql.dbaas.com.br');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define ('DB_CHARSET', 'utf8');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',{Das9LubWlu$XuR8!FB7{73M1H9:0dRg.49dgIGc:>3T! !CA|T<[-7]klF+t]j');
define('SECURE_AUTH_KEY',  'r8;8pe>vK#pw+o|Wqy7YrsYm Qz`@6{j<YjF^-$GwZcK]yCqOx_)hjj{&]>X3(,S');
define('LOGGED_IN_KEY',    'e2DdYpd,1D$<X2%R/h8-<lT*$BySgUA{@h^:a*]lx}2.F+^Ctys8okka4lc=4> d');
define('NONCE_KEY',        'Ja]];zG rCG9Kquh]W4^9?UBYw]fsv|&5I1([WvHCEr1a4~^H*!_hCxh|?`Eo,1:');
define('AUTH_SALT',        'g1#LEhW$l!0;Wv|Xkt#]+}bX&1n/~+i8Ez n$h`f`re3#Gd-vO&T z>LQIGy]U#|');
define('SECURE_AUTH_SALT', 'qB$F{2jZ6![NDEF<&B%,s P7Z_^rl6/(g#_ X|qV%S2!MT@+/eK4$?b<,e]U#W8#');
define('LOGGED_IN_SALT',   'G6o/7-rKFU*:IeM5;v-0xi`aWm3j[LFh/1]?7FYtPg@Fy1 pL)}Gn&xwPTiij`3A');
define('NONCE_SALT',       'j rL4FQ`DZQfl6Y-t,-Sp`$ST_{nNhoX:m,I3EFM:5D(X@wg*3gxRvBPSkOZo/V)');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ir_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
