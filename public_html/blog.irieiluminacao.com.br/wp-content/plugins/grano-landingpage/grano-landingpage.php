<?php
/*
 Plugin Name: Grano Landing Page
 Plugin URI: http://www.granostudio.com.br
 Description: Grano Landing Page
 Author: Grano Studio
 Version: 1.0.0
 Author URI: http://www.granostudio.com.br
 */
 /*--------------------------------------------------------------
  *			Register Grano Plugin
  *-------------------------------------------------------------*/
	//
	// add_action( 'wp_loaded', 'ativarPluginBlog' );
	// function ativarPluginBlog(){
	// 	$ativar = new GranoPluginsAtivos();
	// 	$ativar->ativar('Landing', 'Blog');
	//
	// }

/*--------------------------------------------------------------
 *			Register Landing Page Post Type
 *-------------------------------------------------------------*/
 function cpt_landingpage()
 {
 	$labels = array(
 		'name'                  => _x( 'Landing Page', 'Post type general name', 'granostudio' ),
 		'singular_name'         => _x( 'Landing Page', 'Post type singular name', 'granostudio' ),
 		'menu_name'             => __( 'Landing Page', 'granostudio' ),
 		'name_admin_bar'        => __( 'Add New Landing Page', 'granostudio' ),
 		'parent_item_colon'   	=> __( 'Landing Page anterior:', 'granostudio' ),
 		'all_items'           	=> __( 'Todos as Landing Pages', 'granostudio' ),
 		'view_item'           	=> __( 'Ver fotos da Landing Page', 'granostudio' ),
 		'add_new_item'          => __( 'Add New Landing Page', 'granostudio' ),
 		'add_new'             	=> __( 'Novo Landing Page', 'granostudio' ),
 		'edit_item'           	=> __( 'Editar Grupo de Landing Page', 'granostudio' ),
 		'update_item'         	=> __( 'Update Landing Page', 'granostudio' ),
 		'search_items'        	=> __( 'Procurar Landing Page', 'granostudio' ),
 		'not_found'           	=> __( 'Nenhuma Landing Page encontrado', 'granostudio' ),
 		'not_found_in_trash'  	=> __( 'Nenhuma Landing Page encontrado na lixeira', 'granostudio' )
 		);

 	$args = array(
 		'labels'             	=> $labels,
 		'public'             	=> true,
 		'publicly_queryable' 	=> true,
 		'show_ui'            	=> true,
 		'show_in_menu'       	=> true,
 		'query_var'          	=> true,
 		'rewrite' 						=> true,
 		'capability_type'    	=> 'page',
 		'show_in_admin_bar'   => true,
 		'menu_icon' => 'dashicons-media-document',
 		'can_export'          => true,
 		'has_archive'        	=> true,
 		'hierarchical'       	=> false,
 		'menu_position'      	=> 5,
 		'supports'           	=> array( 'title', 'editor', 'thumbnail' )
 		);

 	register_post_type('landingpage',$args);

 }

 add_action('init','cpt_landingpage');

 // single page
 add_filter('single_template', 'landingpage_single');

function landingpage_single($single) {

    global $wp_query, $post;
    /* Checks for single template by post type */
    if ( $post->post_type == 'landingpage' ) {
        if ( file_exists( plugin_dir_path( __DIR__  ) . 'grano-landingpage/single-landingpage.php' ) ) {

            return plugin_dir_path( __DIR__ ) . 'grano-landingpage/single-landingpage.php';
        }
    }

    return $single;
     wp_reset_postdata();
}
// echo plugin_dir_path( __FILE__ );
