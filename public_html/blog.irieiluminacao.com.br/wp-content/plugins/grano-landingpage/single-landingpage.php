<?php
/**
 * The template for displaying all single portfolio and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<div class="landing-page teaser" ng-app="landingpage" ng-controller="main" ng-class="dataLoaded ? 'leave': 'enter'">
     <main role="main" class="container-fluid main ">
      <div class="row no-gutters">
        <div class="col-md-7 coluna-1" ng-class="dataLoaded ? 'leave': 'enter'">
          <div class="content align-middle">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/landingpage/logo.png" class="img-responsive logo" alt="">
            <h1>NESSA BLACK WEEK A LUSTRES IRIE ESTÁ COMO UMA PROMOÇÃO EXCLUSIVA</h1>
            <!-- <p>
              Em janeiro de 2018, vamos inaugurar o DeR<span style="font-size:0.8rem">OSE</span> Method Vila Nova Conceição. Somos uma escola de técnicas e conceitos que deflagram e desenvolvem talentos, afiam nossos instrumentos de realização e promovem melhoria na qualidade de vida. Nosa casa receberá uma comunidade de influenciadores e empreendedores engajados na transformação da economia, da sociedade e do individuo.
            </p> -->
          </div>
        </div>
        <div class="col-md-5 formulario coluna-2" ng-class="dataLoaded ? 'leave': 'enter'">
          <h1>Receba nossas ofertas da black week em seu email</h1>
          <p>Você que é profissional em arquitetura ou decoração, nós fizemos o Black Week especialmente para você.</p>

          <?php echo do_shortcode('[contact-form-7 id="7" title="Landing Page"]'); ?>

          <div class="endereco small">
            João Lourenço, 137 – Vila Nova Conceição
          </div>
        </div>
      </div>
    </main><!-- /.container -->
</div>

<?php get_footer(); ?>
