<!DOCTYPE html>
<html>

<head>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106582915-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106582915-1');
	</script>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<script>(function(){document.documentElement.className='js'})();</script>

	<link rel="shortcut icon" href="http://www.irieiluminacao.com.br/images/padrao/favicon.png" />

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="/wp-content/themes/irie/css/newblog.css">

	<link rel="stylesheet" href="/css/owl.carousel.min.css">

	<script src="/js/owl.carousel.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<meta property="fb:app_id" content="1959157397697907" />

	<meta property="og:url" content="<?php the_permalink(); ?>" />

	<meta property="og:image" content="<?php the_post_thumbnail( ); ?>" />

  <link href="https://fonts.googleapis.com/css?family=Anton%7CLato:300,400" rel="stylesheet">

	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src=“https://www.googletagmanager.com/gtag/js?id=UA-106582915-1“></script>


  <script>
 (function(i,s,o,g,r,a,m){i[‘GoogleAnalyticsObject’]=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,‘script’,‘https://www.google-analytics.com/analytics.js’,'ga');

 ga(‘create’, ‘UA-106582915-1’, ‘http://blog.irieiluminacao.com.br/’);
 ga(‘send’, ‘pageview’);

</script>

<!-- METATAGS -->
				<meta property="og:url" content="<?php echo get_permalink(); ?>" />
					<?php
					// site title
					$blog_title = get_bloginfo( 'name' );
					if(is_home(	)){
					?>
				<meta property="og:title" content="<?php echo $blog_title; ?>" />
				<?php
				} else{
				?>
				<meta property="og:title" content="<?php echo get_the_title(); ?>" />
				<?php
				}
				?>

				<meta property="og:type" content="website" />

				<?php if(is_home(	)){
							// site description
							$blog_desc = get_bloginfo( 'description' );
					?>
				<meta property="og:description" content="<?php echo $blog_desc; ?>" />
				<?php
				} else{
					?>
				<meta property="og:description" content='<?php echo get_the_content(); ?>' />
				<?php
					}
			 	?>

				<meta property="og:site_name" content="<?php echo $blog_title; ?>" />

</head>

<body <?php body_class(); ?> class="body">

<div class="row" style="margin-left: 0px;margin-right: 0px;">
  <div class="col-md-8 landing-col-left">
    <div class="col-md-6 col-md-offset-1">
      <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Logo-BlackWeek.png" class="img-responsive logo" alt="">
      <h1>NESSA BLACK WEEK A LUSTRES IRIE ESTÁ COMO UMA PROMOÇÃO EXCLUSIVA</h1>
      <p style="font-size: 12px;color: gray;margin-bottom: 40px;font-family: 'Lato', sans-serif;">*No estado de monstruário *Última peça</p>
    </div>
    <div class="col-md-4 col-left-2">
      <p style="margin-bottom: -10px;color: gray;font-size: 20px;font-family: 'Lato', sans-serif;">até</p>
      <p style="margin-bottom: -10px;"><font style="font-size: 125px;margin-top: 0px;font-family: 'Anton', sans-serif;">50</font><font style="font-size: 50px;margin-top: 0px;font-family: 'Anton', sans-serif;">%</font></p>
      <p style="font-size: 22px;color: gray;font-family: 'Lato', sans-serif;line-height: 22px;">de descontos em produtos da Littman*</p>
    </div>
  </div>

  <div class="col-md-4 landing-col-right">
    <h2>RECEBA NOSSAS OFERTAS DA BLACK WEEK EM SEU EMAIL</h2>
    <p style="font-size: 17px;line-height: 22px;margin-bottom: 30px;font-family: 'Lato', sans-serif;">Nós fizemos a Black Week especialmente para você.</p>

    <div class="formulario-landing">
      <?php echo do_shortcode('[contact-form-7 id="296" title="Landing Page"]'); ?>
    </div>
  </div>

</div>

<style>
.landing-col-left{
  background-color: black;
  color: white;
}
.landing-col-left h1{
  font-family: 'Anton', sans-serif;
  font-size: 47px;
	line-height: 60px;
  margin-top: 90px;
  margin-bottom: 105px;
}
.landing-col-left img{
	margin-top: 40px;
}
@media (max-width: 768px) {
	.landing-col-left img{
		margin-top: 0px;
		padding-top: 40px;
		padding-bottom: 50px;
	}
}
@media (max-width: 768px) {
	.landing-col-left h1{
		margin-top: 0px;
		margin-bottom: 20px;
	}
}
.landing-col-left .col-left-2{
	margin-top: 235px;
}
@media (max-width: 768px) {
	.landing-col-left .col-left-2{
		margin-top: 20px;
		padding-bottom: 30px;
	}
}
.landing-col-right h2{
  font-family: 'Anton', sans-serif;
  color: black;
  text-align: center;
  font-size: 28px;
}
.landing-col-right{
  padding: 65px;
  text-align: center;
}
.formulario-landing input{
  padding: 15px;
  width: 100%;
}
.formulario-landing .wpcf7-submit{
  width: 100%;
  background-color: black;
  border: none;
  color: white;
  height: 50px;
}
.wpcf7-acceptance{
	float: left;
	width: auto !important;
	margin-right: 10px !important;
}
.verificar{
	font-size: 11px;
  text-align: left;
	font-family: 'Lato', sans-serif;
}
</style>

</body>
