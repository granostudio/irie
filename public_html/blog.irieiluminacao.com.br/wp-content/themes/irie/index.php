<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('blog'); ?>

<div class="container" style="margin-top:100px;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol> -->


    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'banner',
          'posts_per_page' => 1,
      ));

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="item active" style="height:450px; width:100%">

                  <div class="descricao-banner">
                    <h2><?php the_title() ?></h2>
                    <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                    <a href="<?php echo esc_url( $url ); ?>"><div class="button-banner"><p>Leia Mais</p></div></a>
                  </div>
                  <div class="img-banner">
                    <?php the_post_thumbnail( ); ?>
                  </div>



                </div>
              <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>

        <!--  Segundo Loop -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'banner',
            'offset' => 1,
        ));

        // now check if the query has posts and if so, output their content in a banner-box div
        if ( $query->have_posts() ) { ?>

                <?php while ( $query->have_posts() ) : $query->the_post();
                    $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                  <div class="item" style="height:450px; width:100%">

                    <div class="descricao-banner">
                      <h2><?php the_title() ?></h2>
                      <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                      <a href="<?php echo esc_url( $url ); ?>"><div class="button-banner"><p>Leia Mais</p></div></a>
                    </div>
                    <div class="img-banner">
                      <?php the_post_thumbnail( ); ?>
                    </div>

                  </div>
                <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>

    </div>




    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="background-image:none">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next" style="background-image:none">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="container">

  <div class="container linha_titulo" id="destaques">
      <h2>Destaques da semana</h2>
      <hr class="hr">
  </div>

  <div class="row">
    <?php

       $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
       $args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'paged' => $paged, 'page' => $paged);
       $loop = new WP_Query( $args );

       if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

       <div class="col-md-4 post">
         <a href="<?php echo get_the_permalink(); ?>">
          <div class="img-post" style="width:100%">
            <?php if (has_post_thumbnail()): ?>
              <?php the_post_thumbnail( ); ?>
            <?php else: ?>
              <div class="foto"><img src="<?php echo '/wp-content/themes/irie/images/thumb-blog.png' ?>"></div>
            <?php endif ?>
          </div>
         </a>
          <div class="texto">
            <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
            <p><?php echo the_excerpt_max_charlength(150); ?></p>
            <a href="<?php echo get_the_permalink(); ?>"><div class="botao"><p>+</p></div></a>
          </div>
        </div>

   <?php endwhile; ?>
   <?php endif; ?>

 </div>

 <div class="container linha_titulo">
     <h2>Últimas postagens</h2>
     <hr class="hr2">
 </div>

 <div class="row">
   <?php

      $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged, 'page' => $paged);
      $loop = new WP_Query( $args );

      if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="col-md-4 post">
          <a href="<?php echo get_the_permalink(); ?>">
           <div class="img-post" style="width:100%">
             <?php if (has_post_thumbnail()): ?>
               <?php the_post_thumbnail( ); ?>
             <?php else: ?>
               <div class="foto"><img src="<?php echo '/wp-content/themes/irie/images/thumb-blog.png' ?>"></div>
             <?php endif ?>
           </div>
          </a>
           <div class="texto">
             <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
             <p><?php echo the_excerpt_max_charlength(150); ?></p>
             <a href="<?php echo get_the_permalink(); ?>"><div class="botao"><p>+</p></div></a>
           </div>
         </div>

  <?php endwhile; ?>
  <?php endif; ?>

  <div class="">
        <ul class="pager">
          <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
          <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
        </ul>
      </div>

</div>

</div>

<?php get_footer('blog'); ?>

<style media="screen">

body{
  margin: 0;
  font-family: 'Open Sans', sans-serif;
}
/*=============== Post ===============*/
.post{
  min-height: 550px;
  -webkit-transition: all ease .6s;
  -moz-transition: all ease .6s;
  transition: all ease .6s;
}
.post:hover{
  -webkit-transform: scale(1.03);
  -ms-transform: scale(1.03);
  transform: scale(1.03);
}
.post a{
  color: #D92725;
}
@media (max-width: 414px) {
  .post{
    margin-bottom: 40px;
  }
}
.img-post img{
  width: 100%;
  height: 250px;
}
.foto{
  width: 100%;
  height: 250px;
}
.foto img{
  width: 100%;
}
/*=============== Post ===============*/

/*=========== Hr divisão de posts ===========*/
.linha_titulo{
  position: relative;
  margin-top: 40px;
  margin-bottom: 40px;
}
@media (min-width: 769px) {
  .linha_titulo{
    margin-left: -15px;
  }
}
.linha_titulo .hr{
  position: absolute;
  top: 18px;
  right: 15px;
  width: 69%;
  border: 1px solid #D92725;
}
.linha_titulo .hr2{
  position: absolute;
  top: 18px;
  right: 15px;
  width: 73%;
  border: 1px solid #D92725;
}
.linha_titulo h2{
  background-color: white;
  margin-left: 0;
  color: #D92725;
}
@media (max-width: 768px) {
  .linha_titulo .hr{
    top: 75px;
    width: 100%;
    right: 0px;
  }
  .linha_titulo .hr2{
    top: 75px;
    width: 100%;
    right: 0px;
  }
  .linha_titulo h2{
    margin-left: -15px;
  }
}
@media (min-width: 769px) and (max-width: 991px){
  .linha_titulo .hr{
    width: 55%;
  }
  .linha_titulo .hr2{
    width: 62%;
  }
}
@media (min-width: 992px) and (max-width: 1199px){
  .linha_titulo .hr{
    width: 65%;
  }
  .linha_titulo .hr2{
    width: 70%;
  }
}
@media (max-width: 414px) {
  .hr2{
    width: 90%;
    margin-top: 0px;
  }
}
/*=========== Hr divisão de posts ===========*/

.texto{
  padding: 15px;
  padding-top: 0;
}
.texto p{
  color: black;
  position: relative;
  left: 0;
  display: inherit;
  padding-top: 0px;
  font-size: 14px;
}
.texto a{
  text-decoration: none;
}

/*=========== Botão Leia Mais Post ===========*/
.botao{
  cursor: pointer;
  position: absolute;
  left: 50%;
  margin-left: -23px;
  margin-top: 20px;
  width: 45px;
  height: 45px;
  background-color: #D92725;
  border-radius: 50%;
  -webkit-transition: -webkit-transform .4s ease;
  transition: transform .4s ease;
  bottom: 7%;
}

.botao:hover{
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.botao p{
  position: absolute;
  left: 50%;
  margin-left: -9px;
  color: white;
  font-size: 32px;
  top: 50%;
  margin-top: -24px;
}
/*=========== Botão Leia Mais Post ===========*/

/*=========== Banner ===========*/
.img-banner img{
  width: 70%;
  height: 450px;
  background-repeat: repeat;
  background-size: cover;
  background-position: center;
}
@media (max-width: 768px) {
  .img-banner img{
      width: 100%;
      height: 210px;
  }
}
.descricao-banner{
  width: 30%;
  background-color: #D92725;
  height: 450px;
  right: 0;
  padding: 45px;
  position: absolute;
  color: white;
}
@media (max-width: 599px) {
  .descricao-banner{
    width: 100%;
    padding: 25px;
    padding-top: 0px;
    height: 250px;
    bottom: 0;
  }
}
@media (min-width: 600px) and (max-width: 768px) {
  .descricao-banner{
    width: 100%;
    background: none;
    color: black;
    padding: 25px;
    padding-top: 230px;
  }
}
@media (min-width: 769px) and (max-width: 991px) {
  .descricao-banner{
    width: 30%;
    background-color: #D92725;
    height: 450px;
    right: 0;
    padding: 20px;
    position: absolute;
    color: white;
  }
}
.descricao-banner h2{
	font-size: 30px;
}
@media (max-width: 414px) {
	.descricao-banner h2{
		font-size: 24px;
	}
}
.button-banner{
  width: 100px;
  height: 50px;
  padding-left: 16px;
  padding-top: 13px;
  margin-top: 40px;
  background-color: #D92725;
  cursor: pointer;
  border-radius: 5px;
  color: white;
  border: 2px solid white;
  font-size: 15px;
  -webkit-transition: all ease .6s;
  -moz-transition: all ease .6s;
  transition: all ease .6s;
}
.button-banner p:hover{
  text-decoration: none;
}
.descricao-banner a:hover{
  text-decoration: none;
}
.button-banner:hover{
  -webkit-transform: scale(1.04);
  -ms-transform: scale(1.04);
  transform: scale(1.04);
}
@media (max-width: 599px) {
  .button-banner{
    margin-top: -2px;
  }
}
/*=========== Banner ===========*/

.previous a{
  margin-top: 20px;
  color: #D92725;
}
.next a{
  margin-top: 20px;
  color: #D92725;
}
</style>
