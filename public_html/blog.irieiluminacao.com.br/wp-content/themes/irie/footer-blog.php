<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	<footer>

		<div class="footer2">
			<a href="http://blog.irieiluminacao.com.br"><img src="<?php echo '/wp-content/themes/irie/images/logo.png' ?>"></a>
			<p>www.irieiluminação.com.br - Av Lino Jardim 1183 - Santo André - Lustres Irie</p>
		</div>

		<a id="back-to-top" href="" class="btn btn-lg back-to-top" role="button" title="Clique para retornar ao topo da página" data-toggle="tooltip" data-placement="left"><p>^</p></a>
	</footer>

</div>

<style media="screen">

.footer2{
	width: 100%;
	height: 100px;
	background-color: #D92725;
	text-align: center;
	left: 0;
	bottom: 0;
	top: 100%;
}

.footer2 p{
  color: white;
  font-size: 12px;
  display: inline;
  position: absolute;
  padding-top: 40px;
  left: 41%;
}
@media (max-width: 414px) {
	.footer2 p{
	padding-top: 30px;
	padding-right: 20px;
	}
}

.footer2 img{
  width: 85px;
  display: inline;
  margin-top: 15px;
  position: absolute;
  left: 32%;
}
@media (min-width: 415px) and (max-width: 992px){
	.footer2 img{
	  width: 85px;
	  display: inline;
	  margin-top: 15px;
	  position: absolute;
	  left: 5%;
	}
	.footer2 p{
	  left: 28%;
	}
}
@media (max-width: 414px) {
	.footer2 img{
		width: 23%;
		left: 5%;
	}
}

.back-to-top{
	height: 35px;
	z-index: 9999;
	cursor: pointer;
	position: fixed;
	bottom: 20px;
	right: 20px;
	display: none;
	background-color: #D92725;
	border: 1px solid white;
}
.back-to-top:hover{
	background-color: #D92725;
	border: 1px solid white;
}
.back-to-top p{
	margin: 0;
	font-size: 17px;
	position: absolute;
	top: 50%;
	margin-top: -9px;
	left: 50%;
	margin-left: -4px;
	color: white;
}
.tooltip.left{
	margin-left: -10px;
}

</style>

<script>

// Back to Top ===============================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================

</script>


</body>
</html>
