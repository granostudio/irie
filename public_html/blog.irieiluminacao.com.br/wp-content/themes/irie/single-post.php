<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('blog'); ?>

<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=1959157397697907";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=1932839393611611";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

      <?php if (has_post_thumbnail()): ?>
        <div class="thumb-single">
          <?php the_post_thumbnail( ); ?>
        </div>
      <?php else: ?>
        <div class="thumb"></div>
      <?php endif; ?>
      <!-- / Thumb -->


      <div class="row conteudo">

        <div class="col-sm-8 col-sm-offset-2" style="margin-top: 50px;margin-bottom: 50px;">

          <!-- Blog Post -->

    			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

          <h1 class="title-single"><?php echo get_the_title(); ?></h1>

          <!-- Post Content -->
          <div class="content">
            <?php the_content(); ?>
          </div>

          <hr>

          <!-- Date/Time -->
    			<?php $data_atualização = get_the_modified_date(); ?>

          <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p><br>

          <div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Compartilhar</a></div><br><br>
          <div class="fb-comments" href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>

        <?php endwhile; // end of the loop. ?>

        </div>

      </div>

      <?php get_footer('blog'); ?>


<style>

.footer1{
	width: 100%;
	height: 100px;
	background-color: #D92725;
	text-align: center;
	left: 0;
	bottom: 0;
	top: 100%;
	position: absolute;
}

.footer1 p{
  color: white;
  font-size: 12px;
  display: inline;
  position: absolute;
  padding-top: 40px;
  left: 41%;
}
@media (max-width: 414px) {
	.footer1 p{
	padding-top: 30px;
	padding-right: 20px;
	}
}

.footer1 img{
  width: 85px;
  display: inline;
  margin-top: 15px;
  position: absolute;
  left: 32%;
}
@media (min-width: 415px) and (max-width: 992px){
	.footer1 img{
	  width: 85px;
	  display: inline;
	  margin-top: 15px;
	  position: absolute;
	  left: 5%;
	}
	.footer1 p{
	  left: 28%;
	}
}
@media (max-width: 414px) {
	.footer1 img{
		width: 23%;
		left: 5%;
	}
}

.thumb-single img{
  width: 100%;
  height: 500px;
}
.thumb-single{
  width: 100%;
}
.thumb{
  width: 100%;
  height: 50px;
}
.title-single{
  color: #D92725;
  margin-bottom: 40px;
}
.conteudo{
  background-color: white;
  margin-left: 0px;
  margin-right: 0px;
}

.back-to-top{
	height: 35px;
	z-index: 9999;
	cursor: pointer;
	position: fixed;
	bottom: 20px;
	right: 20px;
	display: none;
	background-color: #D92725;
	border: 1px solid white;
}
.back-to-top:hover{
	background-color: #D92725;
	border: 1px solid white;
}
.back-to-top p{
	margin: 0;
	font-size: 17px;
	position: absolute;
	top: 50%;
	margin-top: -9px;
	left: 50%;
	margin-left: -4px;
	color: white;
}
.tooltip.left{
	margin-left: -10px;
}
.wp-caption-text{
  color: #D92725;
  font-size: 13px;
  padding-bottom: 20px;
}
.size-full img{
		width: 100%;
}
.content p{
  line-height: 28px;
}
.content h2{
  padding-top: 10px;
  padding-bottom: 10px;
}
.content h3{
  padding-top: 10px;
  padding-bottom: 10px;
}
.content h4{
  padding-top: 10px;
  padding-bottom: 10px;
}
.content h5{
  padding-top: 10px;
  padding-bottom: 10px;
}
.content img{
  padding-top: 20px;
  padding-bottom: 10px;
  width: 100%;
}

</style>
