<!DOCTYPE html>
<html>

<head>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106582915-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106582915-1');
	</script>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<script>(function(){document.documentElement.className='js'})();</script>

	<link rel="shortcut icon" href="http://www.irieiluminacao.com.br/images/padrao/favicon.png" />

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="/wp-content/themes/irie/css/newblog.css">

	<link rel="stylesheet" href="/css/owl.carousel.min.css">

	<script src="/js/owl.carousel.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<meta property="fb:app_id" content="1959157397697907" />

	<meta property="og:url" content="<?php the_permalink(); ?>" />

	<meta property="og:image" content="<?php the_post_thumbnail( ); ?>" />

	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src=“https://www.googletagmanager.com/gtag/js?id=UA-106582915-1“></script>


	<script>
 (function(i,s,o,g,r,a,m){i[‘GoogleAnalyticsObject’]=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,‘script’,‘https://www.google-analytics.com/analytics.js’,'ga');

 ga(‘create’, ‘UA-106582915-1’, ‘http://blog.irieiluminacao.com.br/’);
 ga(‘send’, ‘pageview’);

</script>

<!-- METATAGS -->
				<meta property="og:url" content="<?php echo get_permalink(); ?>" />
					<?php
					// site title
					$blog_title = get_bloginfo( 'name' );
					if(is_home(	)){
					?>
				<meta property="og:title" content="<?php echo $blog_title; ?>" />
				<?php
				} else{
				?>
				<meta property="og:title" content="<?php echo get_the_title(); ?>" />
				<?php
				}
				?>

				<meta property="og:type" content="website" />

				<?php if(is_home(	)){
							// site description
							$blog_desc = get_bloginfo( 'description' );
					?>
				<meta property="og:description" content="<?php echo $blog_desc; ?>" />
				<?php
				} else{
					?>
				<meta property="og:description" content='<?php echo get_the_content(); ?>' />
				<?php
					}
			 	?>

				<meta property="og:site_name" content="<?php echo $blog_title; ?>" />

</head>

<body <?php body_class(); ?> class="body">


<div class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid">

        <!-- Menu hamburger Inicio -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#exemplo-navbar-collapse" aria-expanded="false">

                <span class="sr-only">Navegacao</span>
                <span class="icon-bar icon-bar1"></span>
                <span class="icon-bar icon-bar2"></span>
                <span class="icon-bar icon-bar3"></span>

            </button>

            <!-- Título Direita do menu-->
            <a href="http://blog.irieiluminacao.com.br" class="logo2"><img src="<?php echo '/wp-content/themes/irie/images/logo2.png' ?>"></a>

        </div>
        <!-- Menu hamburger Fim -->

        <div class="collapse navbar-collapse" id="exemplo-navbar-collapse">

            <!-- Links Inicio -->
            <ul class="nav navbar-nav navbar-left">

							<?php
								wp_nav_menu( array(
									'menu'              => 'Menu2',
									'theme_location'    => 'Menu2',
									'depth'             => 2,
									'container'         => 'div',
									'container_class'   => 'collapse navbar-collapse',
									'container_id'      => 'bs-example-navbar-collapse-1',
									'menu_class'        => 'nav navbar-nav',
									'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
									'walker'            => new wp_bootstrap_navwalker())
								);
							?>

            </ul>

						<div class="box-buscar">
							<form role="search" method="get" id="searchform">
							  <div class="input-group">
							    <input type="text" class="campo-buscar" name="s" id="s" placeholder="Pesquisar..." />
							    <!-- <div class="input-group-btn">
							      <button class="btn btn-default" type="submit" id="searchsubmit">
										<i class="glyphicon glyphicon-search"></i>
							      </button>
							    </div> -->
							  </div>
							</form>
						</div>


        </div>

    </div>

</div>

<script>
$(document).ready(function(){
    $("button").click(function(){
        $(".navbar-collapse").animate({
            height: 'toggle'
        });
    });
});

$('#menu-item-196').click(function () {
		$('body,html').animate({
				scrollTop: $("#destaques").offset().top -100
		}, 800);
});
</script>

<style media="screen">

body{
	margin: 0;
	font-family: 'Open Sans', sans-serif;
}

.navbar{
	overflow: hidden;
	background-color: white;
	position: fixed;
	top: 0;
	width: 100%;
	z-index: 9999;
	left: 0;
	border: none;
	box-shadow: 0 0 10px rgba(34, 34, 34, 0.8);
}

.logo2{
	padding-right: 35px;
}
.logo2 img{
	width: 70%;
}
.logo2 a:hover:after{
	width: 0;
}

/*=============== Menu ===============*/
.navbar ul{
	padding-top: 14px;
}
@media (max-width: 414px) {
	.navbar ul{
		padding-top: 0px;
	}
}

.navbar-collapse ul li:hover{
	background-color: white;
}

.navbar a {
	float: left;
	display: block;
	color: #656565;
	text-align: center;
	padding: 10px 16px;
	padding-bottom: 16px;
	text-decoration: none;
	font-size: 16px;
	position: relative;
	background-color: white;
}

.navbar a:after{
	position: absolute;
	left: 16px;
	content: '';
	height: 0px;
	background-color: #D92725;
	transition: all 0.2s ease;
	bottom: 3px;
}
.navbar a:hover{
	background-color: white;
}
#menu-item-267 a{
	color: #D92725;
	background-color: white;
}
#menu-item-267 a:after{
	width: 60%;
}
#menu-item-267 a:hover:after{
	height: 5px;
}
#menu-item-196 a{
	color: #656565;
	background-color: white;
}
#menu-item-196 a:after{
	width: 70%;
}
#menu-item-196 a:hover:after{
	height: 5px;
}
#menu-item-197 a{
	background-color: white;
	color: #656565;
}
#menu-item-197 a:after{
	width: 81%;
}
#menu-item-197 a:hover:after{
	height: 5px;
}
#menu-item-198 a{
	background-color: white;
	color: #656565;
}
#menu-item-198 a:after{
	width: 75%;
}
#menu-item-198 a:hover:after{
	height: 5px;
}

.navbar-toggle{
	background-color: #D92725;
	border: none;
}
.navbar-inverse .navbar-toggle:hover {
	background-color: #D92725;
}
.navbar-collapse{
	height: 145px;
	border: none;
}
@media (max-width: 414px) {
	.navbar-inverse .navbar-collapse{
		border-bottom: 2px solid #D92725;
		border-top: none;
	}
	.navbar-collapse ul li{
		display: table;
	}
}

/* Caixa de busca */

.box-buscar{
	width: 23%;
	left: 92%;
	top: 33%;
	position: absolute;
}
@media (max-width: 414px) {
	.box-buscar{
		width: 32%;
		left: 95%;
	}
}
input[type=text] {
	width: 0px;
	padding: 12px 20px 12px 14px;
	border: none;
	-webkit-transition: width 0.5s ease;
	transition: width 0.5s ease;
	top:0; right:0;
	position:absolute;
	background-image: url('/wp-content/themes/irie/images/searchicon.png');
	background-repeat: no-repeat;
	background-size: 40px;
	cursor: pointer;
}
input[type=text]::-webkit-input-placeholder { color:transparent; }
input[type=text]:-moz-placeholder { color:transparent; } /* FF 4-18 */
input[type=text]::-moz-placeholder { color:transparent; } /* FF 19+ */
input[type=text]:-ms-input-placeholder { color:transparent; }

input[type=text]:focus {
    width: 200px;
		outline: none;
		border-bottom: 2px solid red;
		padding: 12px 20px 12px 45px;
}
input:focus::-webkit-input-placeholder { color:black; }
input:focus:-moz-placeholder { color:black; } /* FF 4-18 */
input:focus::-moz-placeholder { color:black; } /* FF 19+ */
input:focus:-ms-input-placeholder { color:black; }

/* Caixa de busca */

/*=============== Menu ===============*/

figure{
	position: relative;
	display: inline-block;
	max-width: 100%;
	padding: 0;
}
/* Figuras especifa do post */
#attachment_230{
	position: absolute;
	right: 0;
	top: 6.8%;
}

@media (max-width: 768px) {
	#attachment_230{
		position: relative;
		right: 0;
		top: 0;
	}
}

#attachment_231{
	position: absolute;
	right: 0;
}

@media (max-width: 768px) {
	#attachment_231{
		position: relative;
		right: 0;
	}
}
/* Figuras especifa do post */

</style>
