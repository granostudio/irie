<?php
function abreviaMes($mes) {
	switch($mes){
		case '01':
			return 'Jan';
		break;
		case '02':
			return 'Fev';
		break;
		case '03':
			return 'Mar';
		break;
		case '04':
			return 'Abr';
		break;
		case '05':
			return 'Mai';
		break;
		case '06':
			return 'Jun';
		break;
		case '07':
			return 'Jul';
		break;
		case '08':
			return 'Ago';
		break;
		case '09':
			return 'Set';
		break;
		case '10':
			return 'Out';
		break;
		case '11':
			return 'Nov';
		break;
		default;
			return 'Dez';
		break;
	}
}
function mes($mes) {
	switch($mes){
		case '01':
			return 'Janeiro';
		break;
		case '02':
			return 'Fevereiro';
		break;
		case '03':
			return 'Março';
		break;
		case '04':
			return 'Abril';
		break;
		case '05':
			return 'Maio';
		break;
		case '06':
			return 'Junho';
		break;
		case '07':
			return 'Julho';
		break;
		case '08':
			return 'Agosto';
		break;
		case '09':
			return 'Setembro';
		break;
		case '10':
			return 'Outubro';
		break;
		case '11':
			return 'Novembro';
		break;
		default;
			return 'Dezembro';
		break;
	}
}

function padraoData($data) {
	$temp = explode('-',$data);
	$ano = $temp[0];
	$mes = $temp[1];
	$dia = $temp[2];
	$data_invertida = $dia . '/' . $mes . '/' . $ano;
	return $data_invertida;
}

function mysqlData($data) {
	$temp = explode('/',$data);
	$dia = $temp[0];
	$mes = $temp[1];
	$ano = $temp[2];
	$data_invertida = $ano . '/' . $mes . '/' . $dia;
	return $data_invertida;
}

function horaMinuto($time) {
	$temp 		= explode(':',$time);
	$hora 		= $temp[0];
	$minuto 	= $temp[1];
	$segundo 	= $temp[2];
	$horaMinuto = $hora.':'.$minuto;
	return $horaMinuto;
}

function primeiroNome($nome) {
    $nomes = explode(' ',$nome);
    return $nomes[0];
}

function proteger($string, $html=false) {
	//verifico se não é uma array
	if(!is_array($string)) {
	}
	
	$string = preg_replace("/(from|select|insert|delete|where|drop table|show tables|\*|--|\\\\)/","",$string);
	$string = str_replace("<script","",$string);
	$string = str_replace("script>","",$string);
	$string = str_replace("<Script","",$string);
	$string = str_replace("Script>","",$string);
	
	$string = trim($string);						//limpa os espaços vazios
	if($html==false) {
		$string = strip_tags($string);				//tira tags html e php
	}
	$string = addslashes($string);					//Adiciona barras invertidas a uma string
	
	return $string;	
}

function enviarEmail(array $config, array $destinatario, $assunto, $body, $isSMTP = 1)
{
    // Instancia o objeto PHPMailer "Que é uma classe que deve ser incluida"
    $mail = new PHPMailer();

    // Se o Email for autenticado "SMTP"
    if($isSMTP == 1) {

        // Captura as configurações enviadas pelo usuário para efeturar a conexão
        $mail->IsSMTP();
        $mail->Host = $config['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $config['username'];
        $mail->Password = $config['password'];
        $mail->Port = $config['port'];
        $mail->SMTPDebug  = 1;
    }

    // Defini as configurações do remetente
    $mail->From     = $config['username'];
    $mail->Sender   = $config['username'];
    $mail->FromName = $config['username'];

    // Defini as configurações do destinário
    $mail->AddAddress($destinatario['email'], $destinatario['name']);

    // Defini que vai conter HTML
    $mail->IsHTML(true);

    // Defini o conteudo da mensagem
    $mail->Subject      = $assunto;
    $mail->Body         = $body;
    $mail->AltBody      = '';

    // Realiza o envio do e-mail
    $sender = $mail->Send();

    // Verifica o retorno do envio do e-mail
    if($sender)
    {
        return true;

    }else{
        return $mail->ErrorInfo;
    }
}
?>