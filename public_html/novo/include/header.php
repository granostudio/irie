	

	<header role="banner">
        <div class="bg_header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <!-- Logo Reduz -->
                        <a class="logo_redux" href="<?php echo URL; ?>" title="<?php echo TITLE_INDEX; ?>">
                            <img src="images/geral/logo-redux.png" alt="<?php echo SITE_NAME; ?>" title="<?php echo SITE_NAME; ?>" />
                        </a>
                        <!-- /Logo Reduz -->
                    </div>

                    <div class="col-lg-10 text-right relative">
                        <ul class="menu_principal">
                            <li>
                                <a class="menu_link" id="conceito" href="javascript:;" title="<?php echo SITE_NAME; ?> | Conceito Irie">Conceito Irie</a>
                            </li>

                            <li>
                                <a class="menu_link"  id="produtos" href="javascript:;" title="<?php echo SITE_NAME; ?> | Produtos">Produtos</a>
                            </li>

                            <li>
                                <a class="menu_link" id="profissional" href="javascript:;" title="<?php echo SITE_NAME; ?> | Profissionais">Profissionais</a>
                            </li>

                            <li>
                                <a class="menu_link" target="_blank" href="http://blog.irieiluminacao.com.br" title="<?php echo SITE_NAME; ?> | Eventos">Eventos</a>
                            </li>

                            <li>
                                <a class="menu_link" id="contato" href="javascript:;" title="<?php echo SITE_NAME; ?> | Contato">Contato</a>
                            </li>

                            <li id="voltar" style="padding-right: 0;">
                                <span>
                                    <img src="images/icones/seta-go.png" alt="Voltar ao Topo" />
                                </span>
                                <a style="padding-right: 0;" href="javascript:;" title="<?php echo SITE_NAME; ?> | Contato">Voltar ao Topo</a>
                            </li>
                        </ul>
                        
                        <div id="social_cont">
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/LustresIrieBelaVista" target="_blank" rel="nofollow" title="Lustres Irie no Facebook">
                                        <img src="images/icones/facebook.png" alt="Facebook" title="Facebook" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/lustresirie" target="_blank" rel="nofollow" title="Lustres Irie no Instagram">
                                        <img src="images/icones/instagram.png" alt="Instagram" title="Instagram" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container relative_cont">
            <div class="row">
                <div class="logo">
                    <a href="<?php echo URL; ?>" title="<?php echo TITLE_INDEX; ?>">
                        <h1 class="fontzero"><?php echo TITLE_INDEX; ?></h1>
                        <img src="images/geral/lustres-irie.png" alt="<?php echo SITE_NAME; ?>" title="<?php echo SITE_NAME; ?>" />
                    </a>
                </div>
            </div>
        </div>

        <!-- Banner -->
        <div class="rev_slider_wrapper">
            <div id="slider_home" class="rev_slider" data-version="5.2">
                <ul>
                    <!-- Banner 01 -->      
                    <li data-transition="fade">          
                        <!-- Imagem do Banner -->
                        <img src="images/banner/banner_1.jpg" alt="Lustres Irie" title="Lustres Irie" />                          
                        <!-- LAYER NR. 1 -->
                        <!--
                        <div class="tp-caption"                             
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0"                             
                            data-start="500"
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            >
                            <a href="javascript:;" title="Saiba Mais" id="sobre">
                                <img src="images/banner/quadrado1.png" alt="Saiba Mais" title="Saiba Mais" />
                            </a>
                        </div>
                        -->
                        <!-- /LAYER NR. 1 -->
                    </li>
                    <!-- Banner 01 -->

                    <!-- Banner 02 -->      
                    <li data-transition="fade">          
                        <!-- Imagem do Banner -->
                        <img src="images/banner/banner_2.jpg" alt="Lustres Irie" title="Lustres Irie" />                          
                        <!-- LAYER NR. 1 -->
                        <!--
                        <div class="tp-caption"                             
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0"                             
                            data-start="500"
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            >
                            <a href="javascript:;" title="Saiba Mais" id="sobre">
                                <img src="images/banner/quadrado1.png" alt="Saiba Mais" title="Saiba Mais" />
                            </a>
                        </div>
                        -->
                        <!-- /LAYER NR. 1 -->
                    </li>
                    <!-- Banner 02 -->
                    
                    <!-- Banner 03 -->      
                    <li data-transition="fade">          
                        <!-- Imagem do Banner -->
                        <img src="images/banner/banner_3.jpg" alt="Lustres Irie" title="Lustres Irie" />                          
                        <!-- LAYER NR. 1 -->
                        <!--
                        <div class="tp-caption"                             
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0"                             
                            data-start="500"
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            >
                            <a href="javascript:;" title="Saiba Mais" id="sobre">
                                <img src="images/banner/quadrado1.png" alt="Saiba Mais" title="Saiba Mais" />
                            </a>
                        </div>
                        -->
                        <!-- /LAYER NR. 1 -->
                    </li>
                    <!-- Banner 03 -->

                    <!-- Banner 04 -->      
                    <li data-transition="fade">          
                        <!-- Imagem do Banner -->
                        <img src="images/banner/banner_4.jpg" alt="Lustres Irie" title="Lustres Irie" />                          
                        <!-- LAYER NR. 1 -->
                        <!--
                        <div class="tp-caption"                             
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0"                             
                            data-start="500"
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            >
                            <a href="javascript:;" title="Saiba Mais" id="sobre">
                                <img src="images/banner/quadrado1.png" alt="Saiba Mais" title="Saiba Mais" />
                            </a>
                        </div>
                        -->
                        <!-- /LAYER NR. 1 -->
                    </li>
                    <!-- Banner 04 -->
                </ul>             
            </div>
        </div>
        <!-- /Banner -->
	</header>