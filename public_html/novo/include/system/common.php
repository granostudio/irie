<?php
session_start();
require "include/classes/Startup.class.php";

$start = new Startup;
$start-> desenvolvimento  = true;

$start-> nome_projeto     = 'Lustres Irie';
$start-> ano_do_projeto   = "2016";
$start-> url_real         = '';
$start-> ssl              = false;

$start-> url_localhost    = 'localhost';
$start-> pasta_localhost  = '/irie';

$start-> url_testserver   = 'upload.procriativo.com.br';
$start-> pasta_testserver = '/cliente/irie/2016/site';

$start-> iniciar();

define("URL_AMIG", false);
define("NOME_PROJETO", $start->criarProjeto());
define("URL", $start->verificarURL());
define("URL_REAL", $start->url_real());
define("COPYRIGHT", $start->copyright());
define("PAGINA", $start->pagina());
define("FAVICON", ''.URL.'/images/padrao/favicon.png');
define("IMG_SITE", ''.URL.'/images/padrao/site.png');
define("SITEMAP", ''.URL.'/upload/sitemap/sitemap.xml');
require "include/system/inc.status.php";
require "include/system/inc.developer.php";
require "include/system/inc.metadados.php";
require "include/system/inc.url_amigavel.php";
?>