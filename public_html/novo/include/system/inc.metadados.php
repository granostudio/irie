<?php
    // OTIMIZACAO
    define("SITE_NAME", "Lustres Irie");
        
    define("TITLE_INDEX", "Lustres Irie");
    define("DESCRIPTION_INDEX", "Desc...");

    define("TITLE_HISTORIA", "Lustres Irie | História");
    define("DESCRIPTION_HISTORIA", "O surgimento da luz artificial aconteceu durante o período da pré-história, quando o homem passou a dominar o fogo. Até o século XIX, o fogo foi utilizado como principal fonte de iluminação noturna, através de velas e lamparinas que utilizavam algum tipo de combustível.");

    define("TITLE_PROF", "Lustres Irie | Profissional");
    define("DESCRIPTION_PROF", "Profissional");

    // REDES SOCIAIS
    define("GOOGLE_PLUS_AUTHOR", "https://plus.google.com/117030312466394988847/posts");
    define("GOOGLE_PLUS_PUBLISHER", "https://plus.google.com/117030312466394988847/posts");
?>