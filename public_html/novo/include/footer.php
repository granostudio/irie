
	<!-- Footer -->
	<footer>
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 text-left">
						<small class="inline mgt_14">
							<?php echo date('Y'); ?> &copy; Lustres Irie - Todos os Direitos Reservados  |  Desenvolvido por <a href="http://www.procriativo.com.br" 
							title="Procriativo - Agência Multimídia" target="_blank" rel="nofollow">Procriativo</a> 
						</small>
					</div>
					<div class="col-lg-6 text-right">
						<span class="loja inline mgr_15 mgt_8">
							Loja Associada:
						</span>
						<span class="inline">
							<a href="http://www.polodesigncenter.com.br/" target="_blank" rel="nofollow" title="Polo Design Center">
								<img src="images/loja-associada/polo-design-center.png" alt="Polo Design Center" title="Polo Design Center" />
							</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- //Footer -->