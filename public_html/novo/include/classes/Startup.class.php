<?php
/*
classe responsavel inicializar o site
desenvolvida por Eugenio Toniati

versão 3.0 em 30.06.2015
- Alterada variavel $desenvolvimento_status para $devenvolvimento
- Alterado o metodo Startup::hash() para Startup::temp_key();
- Deletado o metodo Startup::css();
- Alterado o metodo Startup::redirect() para Startup::redirecionar();
- Alterado o metodo Startup::key_edit() para Startup::editar_serial_key();
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
versão 2.8 em 25.11.2013

- Criada a função Startup::edit_key(); 
- Alterada a função Startup::iniciar(); 
- Adicionado varialvel $ssl
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
versão 2.7 em 26.08.2013

- corrigido bug na função Startup::pagina();
- criado a funcao Startup::info_projeto();
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
versão 2.6 em 13.05.2013

- simplificada função que cria o hash
- projeto não consulta mais webservice para iniciar
- criado console do desenvolvedor
- criado a funcao Startup::url_real()
- criado a funcao Startup::redirect()
- criado a funcao Startup::private_key()
- criado a funcao Startup::start_redirect()
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
versão 2.4
*/
	
class Startup {
	public  $desenvolvimento = false;
	public 	$nome_projeto;
	public 	$ano_do_projeto;
	public 	$url_real;
	public 	$ssl = false;
	public 	$url_localhost;
	public 	$pasta_localhost;
	public 	$url_testserver;
	public 	$pasta_testserver;
	
	
	//Crio a Chave Temporária do Projeto.
	function temp_key() {
		$hash = md5($this->criarProjeto().$this->verificarURL());
		return $hash;
	}
	
	
	//Crio a Chave Privativa do Site.
	function private_key() {
		$key = md5($this->criarProjeto().$this->url_real());
		return $key;
	}
	
	
	//Responsável por Criar o Nome do Projeto.
	function criarProjeto() {
		$nomeProjeto = $this->nome_projeto;
		return $nomeProjeto;
	}


	//Responsável por Criar a URL Final em que o Site será Hospedado.
	function url_real() {
		$url = $this->url_real;
		return $url;
	}
	
	
	//Responsável por Criar o Copyright do Projeto.
	function copyright() {
		if($this->ano_do_projeto < date('Y')) {
			$copyright = $this->ano_do_projeto.' - '.date('Y');
		}
		else {
			$copyright = $this->ano_do_projeto;
		}
	
		return $copyright;
	}
	
	
	//Responsável por Recuperar a URL onde o Projeto está Sendo Utilizado.
	function verificarURL() {
		$url = $_SERVER['SERVER_NAME'];
		
		if(!$this->desenvolvimento) {
			$urlV 	= preg_match('/www./', $url);
			$url 	= $urlV == 1 ? $url : 'www.'.$url;
		}
		
		//Faço verificaçao da url
		if($url == $this->url_localhost) {
			$url = 'http://'.$this->url_localhost.$this->pasta_localhost;
		}
		else if($url == $this->url_testserver) {
			$url = 'http://'.$this->url_testserver.$this->pasta_testserver;
		}
		else{
			$url = $this->ssl == true ? 'https://'.$url : 'http://'.$url;
		}
		
		return $url;
	}
	

	//Responsável por Iniciar a Sessão do Site.
	function iniciar() {	
		if($this->desenvolvimento) {
			$_SESSION[$this->temp_key()] = 1;	
		}
		else {
			$arquivo = 'include/system/serial.txt';
			$key = file_get_contents($arquivo);
			if((string)$key == $this->temp_key()) {
				$_SESSION[$this->temp_key()] = 1;
			}
			else {
				$_SESSION[$this->temp_key()] = 0;
			}
		}
		return true;
	}
	
	
	//Recupera o Nome da Página.
	function pagina() {
		$pagina = $_SERVER ['REQUEST_URI'];
		$pagina = explode('/', $pagina);
		$pagina = end($pagina);
		
		if(strpos($pagina,"?")) {
			$pagina = explode('?', $pagina);
			$pagina = $pagina[0];
		}
				
		return $pagina;
	}
	
	
	//Redireciona Caso Necessário.
	function redirecionar() {
		// Diretório onde encontra-se o arquivo
		$filename = "index.php";
		
		//copio o arquiv
		$copia_arquivo = '___index.php';
		copy($filename, $copia_arquivo);
		
			
		// Verifica se Existe o Arquivo
		if(file_exists($filename)) {
		   $script = file_get_contents($filename);
		} else {
		   $script = "<script language='JavaScript'>location.href='include/system/erro.php?erro=0'</script>";
		}
			
		//Adciona um Novo Texto
		$script = "<script language='JavaScript'>location.href='include/system/erro.php?erro=0'</script>";
		
		//Escrevendo
		$file = @fopen($filename, "w+");
		@fwrite($file, stripslashes($script));
		@fclose($file);
		
		return true;	
	}
	
	
	//Edita a Serial Key
	function editar_serial_key($conteudo) {	
		$arquivo = 'include/system/serial.txt';
		file_put_contents($arquivo, $conteudo);
		return true;
	}
	
	
	//Adiciona os Direitos de Desenvolvimento do Projeto.
	function info_projeto() {
		$arquivo = fopen('include/system/copyright.txt','r');
		while(true) {
			$linha = fgets($arquivo);
			if ($linha == null) 
			break;
			echo $linha;
		}
		fclose($arquivo);
	}
	
	
	//Redireciona Caso Necessário.
	function start_redirect() {
		if($_SESSION[$this->temp_key()] == 0) {
			$url = 'include/system/erro.php?erro=0';
		}
		else {
			$url = 'include/system/erro.php?erro=2';
		}
		return $url;
	}
}
?>