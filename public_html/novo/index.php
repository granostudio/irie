<?php require ('include/system/common.php'); ?>
<!DOCTYPE html>
<html lang="pt-BR" itemscope itemtype="https://schema.org/WebPage">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />

	<title><?php echo TITLE_INDEX; ?></title>
	<meta name="description" content="<?php echo DESCRIPTION_INDEX; ?>" />
    <meta name="robots" content="index, follow" />

    <link rel="base" href="<?php echo URL; ?>" />
    <link rel="canonical" href="<?php echo URL; ?>" />

    <link rel="sitemap" type="application/xml" href="<?php echo SITEMAP; ?>" />
    <link rel="author" href="<?php echo GOOGLE_PLUS_AUTHOR; ?>" />
    <link rel="publisher" href="<?php echo GOOGLE_PLUS_PUBLISHER; ?>" />

    <meta itemprop="name" content="<?php echo TITLE_INDEX; ?>" />
	<meta itemprop="description" content="<?php echo DESCRIPTION_INDEX; ?>" />
	<meta itemprop="image" content="<?php echo IMG_SITE; ?>" />
	<meta itemprop="url" content="<?php echo URL; ?>" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo TITLE_INDEX; ?>" />
    <meta property="og:description" content="<?php echo DESCRIPTION_INDEX; ?>" />
    <meta property="og:image" content="<?php echo IMG_SITE; ?>" />
    <meta property="og:url" content="<?php echo URL; ?>" />
    <meta property="og:site_name" content="<?php echo SITE_NAME; ?>" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="article:author" content="" />
    <meta property="article:publisher" content="" />

	<link rel="shortcut icon" href="<?php echo FAVICON; ?>" />
	
	<link rel="stylesheet" href="css/screen.css" />
	
	<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<script src="js/html5.js"></script>
		<script src="js/respond.js"></script>
	<![endif]-->

	<?php require ('include/system/analyticstracking.php'); ?>

</head>
<body>
	<?php require ('include/header.php'); ?>


	<!-- Conceito -->
	<div id="anc_conceito"></div>

	<section>
		<div class="bg_conceito">
			<div class="container">
				<div class="row relative">
					<div class="bg_conteudo">
						<h1>
							O Conceito Irie
						</h1>
						<p>
							Com seriedade e dedicação, a <strong class="semi_open">Lustres Irie</strong> oferece, a mais de <strong class="semi_open">30 anos</strong> , a melhor solução para a  iluminação de ambientes residenciais e comerciais. Com mais de 800 m² de loja, <strong class="semi_open">o maior show room do ABC</strong>, traz uma incrível gama de opções e luz para as dúvidas relacionadas à compra ideal de seus produtos com peças diferenciadas  e de qualidade. A equipe  de vendedores Irie  está capacitada para orientar  os clientes na escolha de modelos e tipos de luminária, dependendo da necessidade de cada ambiente.
						</p>

						<div class="pull-right">
							<a class="btn_conceito" href="javascript:;" id="veja" title="Conheça um pouco sobre a nossa história">
								Veja os Nossos Produtos
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Conceito -->

	<!-- Produtos -->
	<div id="anc_produtos"></div>
	<section>
		<div class="bg_produtos">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 no-pd-col">
						<?php require ('include/conteudo-produtos.php'); ?>

						<ul class="cont_prod" id="cont_itens">
							<li id="abajures">
								<div class="produtos abajur">
									<div class="overlay"></div>
									<div class="desc_prod">Abajures</div>
								<div>
							</li>
							
							<li id="arandelas">
								<div class="produtos arandela">
									<div class="overlay"></div>
									<div class="desc_prod">Arandelas</div>
								<div>
							</li>

							<li id="colunas">
								<div class="produtos coluna">
									<div class="overlay"></div>
									<div class="desc_prod">Colunas</div>
								<div>
							</li>


							<li id="embutidos">
								<div class="produtos embutido">
									<div class="overlay"></div>
									<div class="desc_prod">Embutidos<br> e Spots</div>
								<div>
							</li>


							<li id="iluminarias">
								<div class="produtos iluminaria">
									<div class="overlay"></div>
									<div class="desc_prod">Luminárias</div>
								<div>	
							</li>

							<li id="jardim">
								<div class="produtos jardim">
									<div class="overlay"></div>
									<div class="desc_prod">Jardim</div>
								<div>	
							</li>

							<li id="contate_nos">
								<div class="produtos produtos_bg">
								<div>	
							</li>

							<li id="led">
								<div class="produtos led">
									<div class="overlay"></div>
									<div class="desc_prod">LED</div>
								<div>
							</li>

							<li id="lustres">
								<div class="produtos lustre">
									
									<div class="overlay"></div>
									<div class="desc_prod">Lustres</div>
								<div>
							</li>

							<li id="pendentes">
								<div class="produtos pendente">
									<div class="overlay"></div>
									<div class="desc_prod">Pendentes</div>
								<div>
							</li>

							<li id="plafons">
								<div class="produtos plafon">
									<div class="overlay"></div>
									<div class="desc_prod">Plafons</div>
								<div>	
							</li>
                            
                            <li class="back_cat">
                                <div class="produtos bg_cat">
                                <div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //Produtos -->

	<!-- Profissionais -->
	<div id="anc_profi"></div>
	<section>
		<div class="bg_prof">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h1>
							Profissionais
						</h1>
						<div class="sep_prof"></div>
						<p>
							Além da questão estética, o arquiteto ou designer de interiores também prepara todo o planejamento da área a ser construída e dos processos que serão feitos, estando em contato direto com todas as etapas da obra. Todos os detalhes são levados em consideração pelo profissional, desde a melhor utilização do espaço até a iluminação mais adequada para o ambiente em questão.<br><br>

							Por isso a <span class="semi_open">Lustres Irie</span> trabalha em parceria com alguns dos mais premiados e respeitados profissionais de <span class="semi_open">arquitetura</span> e <span class="semi_open">design de interiores</span> da região, assegurando que a qualidade dos serviços se estendam além dos produtos à venda. Conheça os parceiros Irie e deixe sua casa perfeita para receber toda a luz que você merece!
						</p>

						<!-- Sanfona Profissionais -->
						<div id="profs">
							<div class="row">
								<div class="col-lg-4" id="prof_1">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/alexandra.png" alt="Alessandra Luz" title="Alexandra Godoi" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Alexandra Godoi</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="profissional.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/barbaraa.png" alt="Bárbara Dundes" title="Bárbara Dundes" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Bárbara Dundes</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="prof-barbara.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/carolina.jpg" style="margin-right:18px;" alt="Carolina Bernardes" title="Carolina Bernardes" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Carolina<br>Bernardes</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="carolina.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="clear"></div>

								<div class="col-lg-4" id="prof_1">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/claudia.png" alt="Cláudia Hypolito" title="Cláudia Hypolito" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Cláudia <br>Hypolito</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="claudia.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/evelin.png" style="margin-right:18px;" alt="Evelin Sayar" title="Evelin Sayar" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Evelin Sayar</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="evelin.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/glaucya.png" style="margin-right:18px;" alt="Glaucya Taraskevicius" title="Glaucya Taraskevicius" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Glaucya<br>Taraskevicius</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="glaucya.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="clear"></div>

								<div class="col-lg-4" id="prof_3">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/gustavo.jpg" alt="Gustavo Palma" title="Gustavo Palma" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Gustavo Palma &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
											<p class="par_prof">Arquiteto</p>
											<a class="prof_gal" href="prof-gustavo.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/marcos.png" style="margin-right:18px;" alt="Marcos Groove" title="Marcos Groove" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Marcos Groove</h2>
											<p class="par_prof">Arquiteto</p>
											<a class="prof_gal" href="marcos.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_3">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/patricia.jpg" alt="Patricia Duarte" title="Patricia Duarte" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Patricia Duarte</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="patricia.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="clear"></div>

								<div class="col-lg-4" id="prof_3">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/durso.jpg" alt="Patricia D'urso" title="Patricia D'urso" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Patrícia D'urso</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="durso.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>
                                

								<div class="col-lg-4" id="prof_3">
									<article style="margin-right:35px;">
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/renata.jpg" style="margin-right:18px;" alt="Renata Neves" title="Renata Neves" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Renata Neves</h2>
											<p class="par_prof">Arquiteta</p>
											<a class="prof_gal" href="renata.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="col-lg-4" id="prof_3">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/silene.png" alt="Silene Padoveze e Cassia Sassi" title="Silene Padoveze e Cassia Sassi" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Silene Padoveze e <br>Cassia Sassi</h2>
											<p class="par_prof">Arquitetos</p>
											<a class="prof_gal" href="prof-silene.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>

								<div class="clear"></div>

								<div class="col-lg-4" id="prof_2">
									<article>
										<div class="cont_prof">
											<span>
												<img src="images/profissionais/tele.jpg" alt="Telê e Andreia" title="Telê e Andreia
" />
											</span>
										</div>
										<div class="cont_prof">
											<h2 style="margin-top: 20px;">Telê e Andreia &nbsp;&nbsp;</h2>
											<p class="par_prof">Arquitetos</p>
											<a class="prof_gal" href="tele.php" title="Saiba Mais">Saiba Mais</a>
										</div>
									</article>
								</div>
							</div>
						</div>
						<!-- //Sanfona Profissionais -->
						
						<a href="javascript:;" id="conheca_prof">Conheça nossos Profissionais</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Profissionais -->

	<!-- Entre em Contato -->
	<div id="anc_contato"></div>
	<section>
		<div class="bg_contato">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h2>
							Entre em Contato
						</h2>
						<div class="separacao mgt_20 mgb_76"></div>
					</div>		
				</div>
				<div class="row">
					<div class="col-lg-5 col-lg-offset-1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9478.110746759083!2d-46.545359848165894!3d-23.66482478761949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce42f680d9cc4d%3A0xb2b0d727e1baf13f!2sAv.+Lino+Jardim%2C+1183+-+Vila+Bastos%2C+Santo+Andr%C3%A9+-+SP%2C+09041-030!5e0!3m2!1spt-BR!2sbr!4v1467809602448" width="100%" height="260" frameborder="0" style="border: 0; margin: 0; padding: 0;" allowfullscreen></iframe>

						<div class="inline_cont mgt_40">
							<address>
								<strong class="semi_open">Telefone:</strong> (11) 4990-1166<br>
                                <strong class="semi_open">Whatsapp:</strong> (11) 98034-6072</strong><br>
								<strong class="semi_open"><a href="mailto:contato@irieiluminacao.com.br?subject=Contato via site">contato@irieiluminacao.com.br</a></strong><br>
								Avenida Lino Jardim, 1183<br>
								Jardim Bela Vista - Sto. André / SP
							<address>
						</div>
						<div class="inline_cont mgl_30 mgt_40">
							<p>
								<strong class="semi_open">Horário de<br> 
								Funcionamento</strong><br>
								Seg - Sex: 9h às 20h<br>
								Sábado: 9h às 18h
							</p>
						</div>
					</div>

					<div class="col-lg-5">
						<div id="retorno" class="text-center"></div>
						<form action="include/send-mail.php" method="post" id="form_contato">
							<input type="text" name="nome" placeholder="Nome" maxlength="200" required />

							<input type="email" name="email" placeholder="E-mail" required />

							<input type="text" class="tel" name="telefone" placeholder="Telefone" />

							<input type="text" name="assunto" placeholder="Assunto" required />

							<textarea name="mensagem" placeholder="Mensagem" rows="6" required></textarea>

							<div class="pull-right">
								<input type="submit" value="Enviar" id="send_form" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Entre em Contato <--></-->
	<?php require ('include/footer.php'); ?>


	<script src="js/jquery.js"></script>
	<script src="js/jquery.themepunch.tools.min.js"></script>
	<script src="js/jquery.themepunch.revolution.min.js"></script>
	<script src="js/outros-scripts.js"></script>
	<script src="js/aplicacao.js"></script>
</body>
</html>