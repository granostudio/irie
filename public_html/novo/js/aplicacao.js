$(function () {
    // Produtos
    /* Iluminarias */
    $('#iluminarias').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_ilu').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_ilu').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Iluminarias */

    /* Jardim */
    $('#jardim').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_jar').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_jar').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Jardim */

    /* LED */
    $('#led').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_led').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_led').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /LED */

    /* Abajures */
    $('#abajures').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_abaj').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_abaj').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Abajures */

    /* Arandelas */
    $('#arandelas').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_aran').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_aran').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Arandelas */

    /* Balizadores */
    $('#balizadores').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_bali').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_bali').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Balizadores */

    /* Colunas */
    $('#colunas').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_col').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_col').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Colunas */

    /* Embutidos */
    $('#embutidos').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_embu').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_embu').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Embutidos */

    /* Espetos */
    $('#espetos').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_esp').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_esp').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Espetos */

    /* Lustres */
    $('#lustres').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_lust').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_lust').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Lustres */

    /* Pendentes */
    $('#pendentes').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_pend').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_pend').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Pendentes */

    /* Plafons */
    $('#plafons').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_plaf').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_plaf').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Plafons */

    /* Ventiladores */
    $('#ventiladores').click(function() {
        $('.cont_prod').fadeOut('fast');
        $('#cont_vent').fadeIn('fast');
        $("html, body").animate({ scrollTop: 900 }, 600);
        return false;
    });

    $('.back_cat').click(function() {
        $('#cont_vent').fadeOut('fast');
        $('#cont_itens').fadeIn('slow');
    });
    /* /Ventiladores */

    /* Valida Form */
	$('.data').mask('11/11/1111');
    $('.horas').mask('00:00');
    $('.cep').mask('99999-999');
    $('.cpf').mask('999.999.999-99', {reverse: true});
    $('.moeda').mask('000.000.000.000.000,00', {reverse: true});
	$('.tel').mask('(00) 000000000');
	$('.celular').mask('(00) 0000-0000',
		{onKeyPress: function(phone, event, currentField, options){
			var new_sp_phone = phone.match(/^(\(1[1-9]\) 9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g);
			new_sp_phone ? $(currentField).mask('(00) 00000-0000', options) : $(currentField).mask('(00) 0000-0000', options)
		}}
	);
	//$("#form_contato").validate();
    /*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
    // Ajax Form
    $('#send_form').click(function() {
        $('#form_contato').ajaxForm({
            target: '#retorno',
            success: function(retorno) {
                $('#retorno').html(retorno);
                $('#form_contato').resetForm();
            },
            beforeSend: function() {
                //$('#form_contato').fadeOut();
                $('#retorno').html('<div class="texto_retorno"><img width="32" height="20" alt="Carregando..." title="Carregando..." src="images/padrao/loader.gif" />&nbsp;&nbsp; Aguarde um Momento...</div>');
            }
        });
    });
	/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

    // Slider
    $("#slider_home").revolution({
        onHoverStop: 'off',
        sliderType   : "standard",
        sliderLayout : "fullscreen",
        delay        : 2000,
        
        navigation   : {
            arrows   : { enable : true }              
        }
    });

    // Voltar ao Topo
    $('#voltar').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 2000);
    });

    // Ref Conceito banner
    $('#sobre').click(function() {
        $('html, body').animate({scrollTop: $('#anc_conceito').offset().top}, 2000);
    });

    //Ancoras do Menu
    $('#conceito').click(function() {
        $('html, body').animate({scrollTop: $('#anc_conceito').offset().top}, 2000);
    });

    $('#profissional').click(function() {
        $('html, body').animate({scrollTop: $('#anc_profi').offset().top}, 2000);
    });

    $('#produtos, #veja').click(function() {
        $('html, body').animate({scrollTop: $('#anc_produtos').offset().top}, 2000);
    });

    $('#contato, #contate_nos').click(function() {
        $('html, body').animate({scrollTop: $('#anc_contato').offset().top}, 2000);
    });

    // Quando o scroll for maior que 300 oculto o logo
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 300) {
            $(".logo").addClass('none');

            $(".logo_redux").addClass('block');
            $("#voltar").addClass('block_voltar');
            //$(".bg_header").addClass('box_shadow_menu');
        }
        else {
            $(".logo").removeClass('none');

            $(".logo_redux").removeClass('block');
            $("#voltar").removeClass('block_voltar');
            //$(".bg_header").removeClass('box_shadow_menu');
        }
    });

    /* Profissionais */
    $("#profs").hide();
    $("#conheca_prof").show();

    $('#conheca_prof').click(function() 
    {
        $('#prof_1, #prof_2, #prof_3').addClass('mgt_60');
        $("#profs").slideToggle();
        $(this).text($(this).text() == 'Conheça nossos Profissionais' ? 'Fechar lista de Profissionais' : 'Conheça nossos Profissionais');
        return false;
    });

    // Fancy Historia
    $(".fancyboxhistoria").fancybox({
        helpers: {
            title  : null, 
            overlay: {
                css    : { 'background': 'rgba(37, 37, 37, 0.95)' },
                locked : false
            }
        },
        'margin'            : 0,
        'padding'           : 0,
        'maxHeight'         : '90%',
        'width'             : '90%',
        'type'              : 'iframe'
    });

    // Galeria Profissional
    $(".prof_gal").fancybox({
        helpers: {
            title  : null, 
            overlay: {
                css    : { 'background': 'rgba(37, 37, 37, 0.95)' },
                locked : false
            }
        },

        'topRatio'          : 0.1,
        'margin'            : 0,
        'padding'           : 0,
        'maxHeight'         : '88%',
        'width'             : '88%',
        'type'              : 'iframe'
    });

    // Galeria dos Produtos
    $(".galeria").fancybox({
        helpers: {
            title  : null, 
            overlay: {
                css    : { 'background': 'rgba(37, 37, 37, 0.95)' },
                locked : false
            }
        },
        beforeShow: function() {
            $(".fancybox-skin").css("backgroundColor", "transparent");
        }
    });
});