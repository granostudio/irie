<?php require ('include/system/common.php'); ?>
<!DOCTYPE html>
<html lang="pt-BR" itemscope itemtype="https://schema.org/WebPage">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />

	<title><?php echo TITLE_PROF; ?></title>
	<meta name="description" content="<?php echo DESCRIPTION_PROF; ?>" />
    <meta name="robots" content="nofollow" />

    <link rel="base" href="<?php echo URL; ?>" />
    <link rel="canonical" href="<?php echo URL; ?>" />

    <link rel="sitemap" type="application/xml" href="<?php echo SITEMAP; ?>" />
    <link rel="author" href="<?php echo GOOGLE_PLUS_AUTHOR; ?>" />
    <link rel="publisher" href="<?php echo GOOGLE_PLUS_PUBLISHER; ?>" />

    <meta itemprop="name" content="<?php echo TITLE_PROF; ?>" />
	<meta itemprop="description" content="<?php echo DESCRIPTION_PROF; ?>" />
	<meta itemprop="image" content="<?php echo IMG_SITE; ?>" />
	<meta itemprop="url" content="<?php echo URL; ?>" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo TITLE_PROF; ?>" />
    <meta property="og:description" content="<?php echo DESCRIPTION_PROF; ?>" />
    <meta property="og:image" content="<?php echo IMG_SITE; ?>" />
    <meta property="og:url" content="<?php echo URL; ?>" />
    <meta property="og:site_name" content="<?php echo SITE_NAME; ?>" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="article:author" content="" />
    <meta property="article:publisher" content="" />

	<link rel="shortcut icon" href="<?php echo FAVICON; ?>" />
	
	<link rel="stylesheet" href="css/screen.css" />
	
	<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<script src="js/html5.js"></script>
		<script src="js/respond.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<h1 class="fontzero"><?php echo TITLE_INDEX; ?></h1>
	</header>

	<!-- Profissional -->
	<div class="container" id="prof_light">
		<div class="row">
			<div class="col-lg-12 text-center no-pd-col">
				<img class="full" src="images/bg/bg-tele.jpg" alt="Profissional" />
			</div>

			<div class="col-lg-6 no-pd-col">
				<img class="prof_thumb" src="images/profissionais/tele.jpg" target="_blank" alt="Telê e Andreia" title="Telê e Andreia" />

				<p>
					Telê e Andreia <span class="open_light">| Arquiteta</span>
				</p>
			</div>

			<div class="col-lg-6 text-right no-pd-col" id="dados">
				<span class="links">
					<a href="mailto:arquitetura.2d@gmail.com" title="Envie um e-mail para o Telê e Andreia">MANDE UM EMAIL |</a>
					<a href="http://www.facebook.com/2d.arquitetura.design/" target="_blank" title="Visite o site do Telê e Andreia">VISITE O SITE</a>
				</span>
			</div>
		</div>		
	</div>
	<!-- //Profissional -->
</body>
</html>