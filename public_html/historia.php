<?php require ('include/system/common.php'); ?>
<!DOCTYPE html>
<html lang="pt-BR" itemscope itemtype="https://schema.org/WebPage">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />

	<title><?php echo TITLE_HISTORIA; ?></title>
	<meta name="description" content="<?php echo DESCRIPTION_HISTORIA; ?>" />
    <meta name="robots" content="nofollow" />

    <link rel="base" href="<?php echo URL; ?>" />
    <link rel="canonical" href="<?php echo URL; ?>" />

    <link rel="sitemap" type="application/xml" href="<?php echo SITEMAP; ?>" />
    <link rel="author" href="<?php echo GOOGLE_PLUS_AUTHOR; ?>" />
    <link rel="publisher" href="<?php echo GOOGLE_PLUS_PUBLISHER; ?>" />

    <meta itemprop="name" content="<?php echo TITLE_HISTORIA; ?>" />
	<meta itemprop="description" content="<?php echo DESCRIPTION_HISTORIA; ?>" />
	<meta itemprop="image" content="<?php echo IMG_SITE; ?>" />
	<meta itemprop="url" content="<?php echo URL; ?>" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo TITLE_INDEX; ?>" />
    <meta property="og:description" content="<?php echo DESCRIPTION_HISTORIA; ?>" />
    <meta property="og:image" content="<?php echo IMG_SITE; ?>" />
    <meta property="og:url" content="<?php echo URL; ?>" />
    <meta property="og:site_name" content="<?php echo SITE_NAME; ?>" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="article:author" content="" />
    <meta property="article:publisher" content="" />

	<link rel="shortcut icon" href="<?php echo FAVICON; ?>" />
	
	<link rel="stylesheet" href="<?php echo URL; ?>/css/screen.css" />
	
	<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<script src="<?php echo URL; ?>/js/html5.js"></script>
		<script src="<?php echo URL; ?>/js/respond.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<h1 class="fontzero"><?php echo TITLE_INDEX; ?></h1>
	</header>

	<!-- História -->
	<div class="container" id="historia">
		<div class="row">
			<div class="col-lg-12 text-center">
				<section>
					<h1>
						Lustres Irie, 20 Anos de Muita Luz
					</h1>
					<div class="sep_hist"></div>
				</section>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4 col-lg-offset-1">
				<p>
					O surgimento da luz artificial aconteceu durante o período da pré-história, quando
					o homem passou a dominar o fogo.
					Até o século XIX, o fogo foi utilizado como principal fonte de iluminação noturna, através de velas e lamparinas que utilizavam algum tipo de combustível.<br><br>

					Em 1879, Thomas Alva Edison (1847-1931) inventor e cientista norte americano, desenvolveu a primeira lâmpada elétrica incandescente viável comercialmente.
				</p>
			</div>

			<div class="col-lg-6 text-center">
				<img src="<?php echo URL; ?>/images/historia/historia.png" alt="<?php echo SITE_NAME; ?> | História" />
			</div>
		</div>
	</div>
	<!-- //História -->
</body>
</html>